const express = require("express");
const router = express.Router();
require("dotenv").config();
//const dbSqlite = require("../db/conexion");
//const routerAdmin = require("../routes/admin");
const {getAll} = require("../../db/conexion");


const IntegranteController = {
    index: async function (req, res) {
        //La pagina de un integrante
        const matricula = req.params.matricula;
        console.log("pag int especifico", matricula);

        //obtener el ID del integrante a partir de su numero de matricula
        const integranteElegido = await getAll("select * from integrantes where matricula = ?", [matricula]);
        console.log("integrae elegido", integranteElegido)

        if(integranteElegido.length != 1)//es porque no vino ningun integrante, o porque vino mas de uno
        {
            console.log("ocurrio un error en el query");
            return res.redirect("/");
        }


        //hacer un select de inte
        //const media = await getAll("select * from media",);
        //const tipoMedia = await getAll("select * from tipoMedia",);
        console.log("matri");
        //if (matricula.includes(matricula)) {
            //const integranteFilter = await getAll("select * from integrantes where matricula = ?", [matricula]);
            const integranteList = await getAll("select * from integrantes",);
            const mediaFilter = await getAll("select * from media where idIntegrantes = ?", [integranteElegido[0].id]);
            //const media = await getAll("select * from media",);
            res.render('integrante', {
                integrante: integranteElegido,
                integrantes: integranteList, //para el nav enviar todos los integrantes
                //tipoMedia: tipoMedia,
                medias: mediaFilter, //para los medias especificos de un integrante es media Filter
                //medias: media,
                materia: process.env.MATERIA,
                alumno: process.env.ALUMNO,
                repositorio: process.env.ENLACE_REPOSITORIO
            });
        //} else {
          //  next();
        //}
    }
};

module.exports = IntegranteController;