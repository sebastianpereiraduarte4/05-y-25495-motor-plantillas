const express = require('express');
//const {getAll, run, matriculaExistente, getLastId} = require("../db/conexion");
const router = express.Router();
//const fs = require('fs').promises;
const multer = require('multer');
const upload = multer({ dest: "./public/assets/" });
const fileUpload = upload.single("src");
const AdminIntegrantesController = require("../controllers/admin/integrantes.controller");
const AdminTipoMediaController = require("../controllers/admin/tipo_media.controller");
const AdminMediaController = require("../controllers/admin/media.controller");


//prefijo /admin
router.get("/", (req, res) => {
    console.log("en admin");
    res.render("admin/index");
});

router.get("/integrantes/listar", AdminIntegrantesController.index);

router.get("/integrantes/crear", AdminIntegrantesController.crearForm);

router.post("/integrantes/create", AdminIntegrantesController.store);

router.post('/integrantes/delete/:matricula', AdminIntegrantesController.destroy);

router.get('/integrantes/edit/:matricula', AdminIntegrantesController.edit);


///////////////////////////////////////////////////////////////////////////////////////////
//MEDIA
router.get("/media/listar", AdminMediaController.index);

router.get("/media/crear", AdminMediaController.crearForm);

router.post("/media/create", fileUpload, AdminMediaController.store);

router.post('/media/delete/:id', AdminMediaController.destroy);


///////////////////////////////////////////////////////////////////////
//TIPO DE MEDIA
router.get("/tipo_media/listar", AdminTipoMediaController.index);

router.get("/tipo_media/crear", AdminTipoMediaController.crearForm);

router.post("/tipo_media/create", AdminTipoMediaController.store);

router.post('/tipo_media/delete/:id', AdminTipoMediaController.destroy);


module.exports = router;