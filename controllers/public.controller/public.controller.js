const express = require("express");
const router = express.Router();
require("dotenv").config();
//const dbSqlite = require("../db/conexion");
//const routerAdmin = require("../routes/admin");
const {getAll} = require("../../db/conexion");


const PublicController = {
    index: async function (req , res) {
        //home
        const rows = await getAll("select * from integrantes",);
        console.log(rows);
        res.render("index", {
            integrantes: rows,
            materia: process.env.MATERIA,
            alumno: process.env.ALUMNO,
            repositorio: process.env.ENLACE_REPOSITORIO
        });
    }
};
module.exports = PublicController;