const express = require('express');
const {getAll, run, matriculaExistente, getLastId} = require("../../db/conexion");
const router = express.Router();
const fs = require('fs').promises;
const multer = require('multer');
const upload = multer({ dest: "./public/assets/" });
const fileUpload = upload.single("src");

//index - listado


//create - formulario de creación


//store - método de guardar en la base de datos


//show - formulario de ver un registor


//update - método de editar un registro


//edit - formulario de edición


//destroy - operación de eliminar un registro

const TipoMediaController = {

    index: async function (req, res) {
        const tipoMedia =  await getAll("select * from tipoMedia where activo = 1 order by id");
        res.render("admin/tipo_media/index", {
            tipoMedia: tipoMedia
        });
    },

    crearForm: function(req, res) {
        res.render("admin/tipo_media/crearForm", {
            nombre: req.query.nombre,
        });
    },

    store: async function(req, res) {
        let errores = [];

        const lastId =  await getLastId('tipoMedia');
        const newId = lastId + 1;

        if (req.body.nombre === '' || req.body.nombre.length > 50) {
            errores.push('¡El nombre no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }

        if (req.body.activo === undefined) {
            errores.push('¡Debe seleccionar un estado (activo o inactivo)!');
        }

        if (errores.length > 0) {
            res.redirect(`/admin/tipo_media/crear?error=${encodeURIComponent(errores.join(';'))}&nombre=${encodeURIComponent(req.body.nombre)}`);
        } else {
            try {
                await run("insert into tipoMedia (id, nombre, activo) values (?, ?, ?)",
                    [
                        newId,
                        req.body.nombre,
                        req.body.activo
                    ]);
                res.redirect(`/admin/tipo_media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/tipo_media/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },


    show: function(req, res) {

    },

    update: function () {

    },

    edit: function(req, res) {

    },

    destroy:async function (req, res) {
    // Método destroy para borrado lógico
        const id = req.params.id;
        console.log("id tMedia", id);

        try {
            const sql = 'UPDATE tipoMedia SET activo = 0 WHERE id = ?';
            await run(sql, [id]);
            res.redirect('/admin/tipo_media/listar');
        } catch (error) {
            console.error('Error al actualizar el estado del tipo de media:', error);
            res.status(500).json({ message: 'Error al actualizar el estado del tipo de media' });
        }
    },


};

module.exports = TipoMediaController;