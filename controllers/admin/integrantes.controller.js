const express = require('express');
const {getAll, run, matriculaExistente, getLastId} = require("../../db/conexion");
//const router = express.Router();
//const fs = require('fs').promises;
const multer = require('multer');
const upload = multer({ dest: "./public/assets/" });
//const fileUpload = upload.single("src");

//index - listado


//create - formulario de creación


//store - método de guardar en la base de datos


//show - formulario de ver un registor


//update - método de editar un registro


//edit - formulario de edición


//destroy - operación de eliminar un registro


const IntegrantesController = {
    //definición del objeto
    index: async function (req, res) {
        console.log("in index");
        const integrantes = await getAll("select * from integrantes where activo = 1 order by id");
        res.render("admin/integrantes/index", {
            integrantes: integrantes
        });
    },

    crearForm: function (req, res) {
        res.render("admin/integrantes/crearForm", {
            matricula: req.query.matricula,
            nombre: req.query.nombre,
            apellido: req.query.apellido,
            activo: req.query.activo
        });
    },

    store: async function (req, res) {
        let errores = [];


        if (req.body.matricula === '') {
            errores.push('¡La matrícula no puede estar vacía!');
        }

        if (await matriculaExistente(req.body.matricula)) {
            errores.push('¡La matrícula ya existe!');
        }

        if (req.body.nombre === '' || req.body.nombre?.length > 50) {
            errores.push('¡El nombre no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }

        if (req.body.apellido === '' || req.body.apellido?.length > 50) {
            errores.push('¡El apellido no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }

        if (req.body.activo === undefined) {
            errores.push('¡Debe seleccionar un estado (activo o inactivo)!');
        }

        if (errores.length > 0) {
            res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent(errores.join(';'))}
        &matricula=${encodeURIComponent(req.body.matricula)}
        &nombre=${encodeURIComponent(req.body.nombre)}
        &apellido=${encodeURIComponent(req.body.apellido)}`);
        } else {
            try {
                await run("insert into integrantes (matricula, nombre, apellido, activo) values (?, ?, ?, ?)",
                    [
                        req.body.matricula,
                        req.body.nombre,
                        req.body.apellido,
                        req.body.activo
                    ]);
                res.redirect(`/admin/integrantes/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },


    show: function (req, res) {

    },
//operación de edición
    update: async function (req, res) {


    },

    //el metodo que renderiza la vista que contiene el formulacio de edición
    //ademas de enciarle los datos que tiene que precargar en dicho formulario
    edit: async function (req, res) {
        const idIntegrante = parseInt(req.params.id);
        console.log("id integrante", id);

        //una vez que tengamis el id del registro a editar
        //lo primero es realizar un select para obtener los datos
        db.get(
        "select * from integrantes where id = ?", [idIntegrante],
            (error,integrante) => {
                if (err) {
                    console.error(
                        "error al obtener a los integrantes",
                        err.message
                    );
                    res.status(500).send("Error interno del sevidor");
                }
                res.render("admin/integrantes/editForm", {
                    integrante: integrante,
                });
            }

        );

    //y luego renderizar la vista del formulatio de edicion
    res.redirect("/admin/integrantes/listar");
},

    destroy:async function (req, res) {
// Método destroy para borrado lógico
            const matricula = req.params.matricula;
            console.log("matricula", matricula);

            try {
                const sql = 'UPDATE integrantes SET activo = 0 WHERE matricula = ?';
                await run(sql, [matricula]);
                res.redirect('/admin/integrantes/listar');
            } catch (error) {
                console.error('Error al actualizar el estado del integrante:', error);
                res.status(500).json({ message: 'Error al actualizar el estado del integrante' });
            }
        },
};



module.exports = IntegrantesController;