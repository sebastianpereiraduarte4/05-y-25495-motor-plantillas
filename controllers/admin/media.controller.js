const express = require('express');
const {getAll, run, matriculaExistente, getLastId} = require("../../db/conexion");
const router = express.Router();
const fs = require('fs').promises;
const multer = require('multer');
const upload = multer({ dest: "./public/assets/" });
const fileUpload = upload.single("src");

//index - listado


//create - formulario de creación


//store - método de guardar en la base de datos


//show - formulario de ver un registor


//update - método de editar un registro


//edit - formulario de edición


//destroy - operación de eliminar un registro

const MediaController = {

    index: async function (req, res) {
        const media =  await getAll(`
        SELECT media.*, tipoMedia.nombre as tipoMediaNombre, integrantes.nombre as integranteNombre, integrantes.apellido as integranteApellido
        FROM media
        LEFT JOIN tipoMedia ON media.idTipomedia = tipoMedia.id
        LEFT JOIN integrantes ON media.idIntegrante = integrantes.matricula
        WHERE media.activo = 1
        ORDER BY media.id
    `);
        res.render("admin/media/index", {
            media: media,
        });
    },

    crearForm: async function (req, res) {
        const integrantes = await getAll("select * from integrantes where activo = 1 order by nombre");
        const tipoMedia = await getAll("select * from tipoMedia where activo = 1 order by nombre");
        res.render("admin/media/crearForm", {
            integrantes: integrantes,
            tipoMedia: tipoMedia,
            tipoMediaSeleccionado: req.query.tipoMedia,
            url: req.query.url,
            alt: req.query.alt,
            integranteSeleccionado: req.query.integrante,
        });
    },

    store: async function (req, res) {
        let errores = [];

        const lastId =  await getLastId('media');
        const newId = lastId + 1;

        if (req.body.tipoMedia === '' || req.body.tipoMedia === undefined) {
            errores.push('¡Debe seleccionar un tipo de media!');
        }

        if (req.body.integrante === '' || req.body.integrante === undefined) {
            errores.push('¡Debe seleccionar un integrante!');
        }

        if (req.body.url && req.file) {
            errores.push('¡No puedes agregar tanto URL como SRC al mismo tiempo!');
        }

        if (req.body.activo === undefined) {
            errores.push('¡Debe seleccionar un estado (activo o inactivo)!');
        }

        if (errores.length > 0) {
            res.redirect(`/admin/media/crear?error=${encodeURIComponent(errores.join(';'))}
            &tipoMedia=${encodeURIComponent(req.body.tipoMedia)}
            &url=${encodeURIComponent(req.body.url)}
            &alt=${encodeURIComponent(req.body.alt)}
            &integrante=${encodeURIComponent(req.body.integrante)}`);
        } else {
            let srcPath = '';
            if (req.file) {
                var tpm_path = req.file.path;
                var destino = "public/assets/" + req.file.originalname;
                try {
                     fs.rename(tpm_path, destino);
                    srcPath = "/assets/" + req.file.originalname;
                } catch (err) {
                    return res.sendStatus(500);
                }
            }
            try {
                 await run("insert into media (id, url, src, alt, idTipomedia, idIntegrante, activo) values (?, ?, ?, ?, ?, ?, ?)",
                    [
                        newId,
                        req.body.url,
                        srcPath,
                        req.body.alt,
                        req.body.tipoMedia,
                        req.body.integrante,
                        req.body.activo,
                    ]);
                res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/media/listar?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },


    show: function(req, res) {

    },

    update: function () {

    },

    edit: function(req, res) {

    },

    destroy: async function (req, res) {
// Método destroy para borrado lógico
        const id = req.params.id;
        console.log("id de media", id);

        try {
            const sql = 'UPDATE media SET activo = 0 WHERE id = ?';
            await run(sql, [id]);
            res.redirect('/admin/media/listar');
        } catch (error) {
            console.error('Error al actualizar el estado de la media:', error);
            res.status(500).json({ message: 'Error al actualizar el estado de la media' });
        }
    },
};

module.exports = MediaController;