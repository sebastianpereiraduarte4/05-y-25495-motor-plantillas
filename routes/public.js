const express = require("express");
const router = express.Router();
require("dotenv").config();
const {getAll} = require("../db/conexion");
const PublicIntegranteController = require("../controllers/public.controller/integrante.controller");
const PublicCursoController = require("../controllers/public.controller/curso.controller");
const PublicWorldCloudController = require("../controllers/public.controller/world_cloud.controller");
const PublicController = require("../controllers/public.controller/public.controller");

router.get("/", PublicController.index);//home

router.get("/integrante/:matricula", PublicIntegranteController.index);//int especif

router.get("/paginas/curso.html", PublicCursoController.curso);

router.get("/paginas/word_cloud.html", PublicWorldCloudController.worldCloud);

//Exportamos el router
module.exports = router;